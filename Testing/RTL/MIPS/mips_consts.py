
from dataclasses import dataclass


class Opcodes:
    # basic
    op_special = 0b000000
    op_addi = 0b001000
    op_addiu = 0b001001
    op_andi = 0b001100
    op_ori = 0b001101
    op_slti = 0b001010
    op_sltiu = 0b001011
    op_xori = 0b001110
    # branch jumps
    op_regimm = 0b000001
    op_beq = 0b000100
    op_bne = 0b000101
    op_bgtz = 0b000111
    op_blez = 0b000110
    op_j = 0b000010
    op_jal = 0b000011

    # mem i/f commands
    op_lb = 0b100000
    op_lbu = 0b100100
    op_lh = 0b100001
    op_lhu = 0b100101
    op_lui = 0b001111
    op_lw = 0b100011
    op_lwl = 0b100010
    op_lwr = 0b100110

    op_sb = 0b101000
    op_sh = 0b101001
    op_sw = 0b101011
    op_swl = 0b101010
    op_swr = 0b101110


class RegImm:
    regimm_bgez = 0b00001
    regimm_bgezal = 0b10001
    regimm_bltz = 0b00000
    regimm_bltzal = 0b10000


class Funct:
    # ------------special subopcodes-----------------------
    funct_add = 0b100000
    funct_addu = 0b100001
    funct_sub = 0b100010
    funct_subu = 0b100011

    funct_and = 0b100100
    funct_nor = 0b100111
    funct_or = 0b100101
    funct_xor = 0b100110

    funct_sll = 0b000000
    funct_sllv = 0b000100
    funct_sra = 0b000011
    funct_srav = 0b000111
    funct_srl = 0b000010
    funct_srlv = 0b000110

    funct_slt = 0b101010
    funct_sltu = 0b101011

    funct_break = 0b001101
    funct_syscall = 0b001100

    funct_jalr = 0b001001
    funct_jr = 0b001000

    funct_mult = 0b011000
    funct_multu = 0b011001
    funct_mfhi = 0b010000
    funct_mflo = 0b010010
    funct_mthi = 0b010001
    funct_mtlo = 0b010011


class AluOps:
    aluop_add = 0b0000
    aluop_sub = 0b0001
    aluop_special = 0b0010
    aluop_addu = 0b0011
    aluop_subu = 0b0100
    aluop_and = 0b0101
    aluop_or = 0b0110
    aluop_xor = 0b0111
    aluop_slt = 0b1000
    aluop_sltu = 0b1001
    aluop_lui = 0b1010
    aluop_slte = 0b1011


class Alu:
    alu_nop = 0b00000
    alu_add = 0b00001
    alu_addu = 0b10001
    alu_sub = 0b00010
    alu_subu = 0b10010
    alu_nor = 0b00100
    alu_xor = 0b00101
    alu_slt = 0b00110
    alu_sltu = 0b10110
    alu_and = 0b00111
    alu_or = 0b01000
    alu_mult = 0b11000
    alu_multu = 0b11001
    alu_mfhi = 0b11100
    alu_mflo = 0b11101
    alu_sll = 0b01010
    alu_srl = 0b01011
    alu_sra = 0b01001
    alu_slte = 0b01110


class MemOp:
    memop_word = 0b00
    memop_byte = 0b01
    memop_half = 0b10


class Btype:
    btype_beq = 0b000
    btype_bne = 0b001
    btype_bgez = 0b010
    btype_bltz = 0b011
    btype_bgtz = 0b100
    btype_blez = 0b101


@dataclass
class t_control_to_ex:

    link: bool = 0
    jump: bool = 0
    aluop: bool = 0
    alusrc: bool = 0
    regdst: bool = 0
    memop: MemOp = MemOp.memop_word
    branchtype: Btype = Btype.btype_beq
    cp_reg_write: bool = 0

    def _to_reg(self):
        return int(f"{self.link:b}{self.jump:b}{self.aluop:04b}{self.alusrc:b}{self.regdst:b}{self.memop:02b}{self.branchtype:03b}{self.cp_reg_write:b}", 2)

    def __eq__(self, other):
        return self._to_reg() == other

    def __str__(self):
        return f"link: {self.link}, jump: {self.jump}, aluop: {self.aluop}, alusrc: {self.alusrc}, \
            regdst: {self.regdst}, memop: {self.memop}, branchtype: {self.branchtype}, cp_reg_write: {self.cp_reg_write}"


@dataclass
class t_control_to_m:
    memunsigned: bool = 0
    memwrite: bool = 0
    memread: bool = 0
    cp0_regread: bool = 0
    branch: bool = 0

    def _to_reg(self):
        return int(f"{self.memunsigned:b}{self.memwrite:b}{self.memread:b}{self.cp0_regread:b}{self.branch:b}", 2)

    def __eq__(self, other):
        return self._to_reg() == other

    def __str__(self):
        return f"memunsigned: {self.memunsigned}, memwrite: {self.memwrite}, memread: {self.memread}, cp0_regread: {self.cp0_regread}, branch: {self.branch}"


@dataclass
class t_control_to_wb:
    memtoreg: bool = 0
    regwrite: bool = 0

    def _to_reg(self):
        return int(f"{self.memtoreg:b}{self.regwrite:b}", 2)

    def __eq__(self, other):
        return self._to_reg() == other

    def __str__(self):
        return f"memtoreg: {self.memtoreg}, regwrite: {self.regwrite}"

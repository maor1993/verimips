import cocotb
from cocotb.handle import HierarchyObject
from cocotb.clock import Clock
from Testing.RTL.tb_utils import wait_clocks
import os

if_dir = os.path.dirname(__file__)
MEMFILE_PATH = os.path.join(if_dir, "if_stage_top_tb_i_mem.txt")


g_i_mem = []


def read_i_mem_file(mem_var):
    try:
        with open(MEMFILE_PATH, 'r') as f:
            for line in f.readlines():
                mem_var.append(int(line[:-1], base=16))
    except FileNotFoundError:
        print("failed to find memory file, exiting.")
        exit(1)


def load_i_mem(dut, mem_var):
    dut._log.info("Writing instruction memory")
    for idx, data in enumerate(mem_var):
        dut.i_mem_dut.mem[idx].value = data


def assert_load_pc(dut, addr):
    assert dut.id_next_pc.value == addr, f"id_next_pc is not {addr}, got {dut.id_next_pc.value}"


@cocotb.test()
async def basic_io(dut: HierarchyObject):
    read_i_mem_file(g_i_mem)
    load_i_mem(dut, g_i_mem)

    dut.test_number.value = 1
    # Create a 10ns period clock on port clk
    clock = Clock(dut.clk, 10, units="ns")
    await cocotb.start(clock.start())  # Start the clock

    # set all signals to basic state
    dut.id_stall.value = 0
    dut.mem_branch.value = 0
    dut.mem_new_pc.value = 0
    # reset dut for 3 clocks
    dut.rstn.value = 0
    await wait_clocks(dut.clk, 3)
    dut.rstn.value = 1
    await wait_clocks(dut.clk, 2)
    dut._log.info("Case 1.1:Checking Basic functionallity")
    assert dut.id_next_pc.value == 8, f"id_next_pc is not 8, got {dut.id_next_pc.value.integer}"
    assert dut.id_instruction.value == g_i_mem[1], f"expected instruction is incorrect, got {dut.id_instruction.value.buff}"
    assert dut.id_read_reg1.value == 0b10101, "id_read_reg1 is not 10101"
    assert dut.id_read_reg2.value == 0b01101, "id_read_reg2 is not 01101"
    await wait_clocks(dut.clk, 1)
    assert dut.id_instruction.value == g_i_mem[2], "id_instruction is not 0"
    dut._log.info("Case 1.2:Resetting dut")

    # reset dut for 3 clocks
    dut.rstn.value = 0
    await wait_clocks(dut.clk, 3)
    dut.rstn.value = 1
    await wait_clocks(dut.clk, 2)

    assert dut.id_next_pc.value == 8, f"id_next_pc is not 8, got {dut.id_next_pc.value}"
    assert dut.id_instruction.value == g_i_mem[1], f"expected instruction is incorrect, got {dut.id_instruction.value}"


@cocotb.test()
async def stall_event(dut: HierarchyObject):
    load_i_mem(dut, g_i_mem)
    dut.test_number.value = 2
    # Create a 10ns period clock on port clk
    clock = Clock(dut.clk, 10, units="ns")
    await cocotb.start(clock.start())  # Start the clock

    # reset dut for 3 clocks
    dut.rstn.value = 0
    await wait_clocks(dut.clk, 3)
    dut.rstn.value = 1
    await wait_clocks(dut.clk, 4)

    assert_load_pc(dut, 12)

    dut._log.info("Case 2.1: performing stall")
    dut.id_stall.value = 1
    await wait_clocks(dut.clk, 10)

    assert_load_pc(dut, 16)

    dut._log.info("Case 2.2: releasing stall")
    dut.id_stall.value = 0
    await wait_clocks(dut.clk, 2)
    assert_load_pc(dut, 20)


@cocotb.test()
async def branch_event(dut: HierarchyObject):
    load_i_mem(dut, g_i_mem)
    dut.test_number.value = 3
    # Create a 10ns period clock on port clk
    clock = Clock(dut.clk, 10, units="ns")
    await cocotb.start(clock.start())  # Start the clock

    dut._log.info("Case 3.1: performing branch")
    dut.mem_new_pc.value = 0x13fc
    dut.mem_branch.value = 1
    await wait_clocks(dut.clk, 1)
    dut.mem_branch.value = 0
    await wait_clocks(dut.clk, 2)

    assert_load_pc(dut, 0x1400)
    assert dut.id_instruction.value == 0xabcdeeee, f"expected instruction is incorrect, got {dut.id_instruction.value}"


if __name__ == "__main__":
    MEMFILE_PATH = "Testing/RTL/MIPS/IF/if_stage_top_tb_i_mem.txt"
    junk = ""
    load_i_mem(junk)

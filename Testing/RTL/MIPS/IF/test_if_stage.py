from cocotb_test.simulator import run


def test_if_stage_top():
    run(verilog_sources=["RTL/MIPS/IF/if_stage_top.sv"],
        toplevel="if_stage_top",
        module="if_stage_top_tb",
        compile_args=["-grelative-include"])


if __name__ == "__main__":
    test_if_stage_top()

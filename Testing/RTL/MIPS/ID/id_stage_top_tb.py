from cocotb.triggers import ClockCycles
from cocotb.clock import Clock
import cocotb
from Testing.RTL.tb_utils import wait_for_debugger, update_test_number  # noqa E501
from Testing.RTL.MIPS.ID.id_stage_top_mapping import IDStageTopOutput, IdStageTopMaps

# test cases we need for ID stage
# validate instructions pipe correctly
# validate hazzard detection
# validate stall events
# imm positive negative works


def test_all(dut, expected: IDStageTopOutput):
    assert dut.ex_imm.value == expected.ex_imm, f"Expected {expected.ex_imm} but got {dut.ex_imm.value}"
    assert dut.ex_rs.value == expected.ex_rs, f"Expected {expected.ex_rs} but got {dut.ex_rs.value}"
    assert dut.ex_rt.value == expected.ex_rt, f"Expected {expected.ex_rt} but got {dut.ex_rt.value}"
    assert dut.ex_rd.value == expected.ex_rd, f"Expected {expected.ex_rd} but got {dut.ex_rd.value}"
    assert dut.ex_control_to_ex.value == expected.ex_control_to_ex, f"Expected {expected.ex_control_to_ex} but got {dut.ex_control_to_ex.value}"
    assert dut.ex_control_to_m.value == expected.ex_control_to_m, f"Expected {expected.ex_control_to_m} but got {dut.ex_control_to_m.value}"
    assert dut.ex_control_to_wb.value == expected.ex_control_to_wb, f"Expected {expected.ex_control_to_wb} but got {dut.ex_control_to_wb.value}"


@cocotb.test()
async def basic_instructions(dut):
    # Create a 10ns period clock on port clk
    clock = Clock(dut.clk, 10, units="ns")
    await cocotb.start(clock.start())  # Start the clock
    dut.if_instruction.value = 0
    dut.ex_memread.value = 0
    dut.ex_rt_in.value = 0
    dut.mem_branch.value = 0
    dut.wb_regwrite.value = 0
    dut.wb_writereg.value = 0
    dut.rstn.value = 0
    await ClockCycles(dut.clk, 3)
    dut.rstn.value = 1
    await ClockCycles(dut.clk, 1)
    # test case 1.1
    update_test_number(dut, 1)
    # wait_for_debugger()
    for idx, key in enumerate(IdStageTopMaps.one_clock_tests.keys()):
        test_ut: IdStageTopMaps = IdStageTopMaps.one_clock_tests[key]
        dut._log.info(f"running stimulus for opcode {key}")
        dut.if_instruction.value = test_ut["In"].if_instruction
        dut.ex_memread.value = test_ut["In"].ex_memread
        dut.ex_rt_in.value = test_ut["In"].ex_rt_in
        dut.mem_branch.value = test_ut["In"].mem_branch
        dut.wb_regwrite.value = test_ut["In"].wb_regwrite
        dut.wb_writereg.value = test_ut["In"].wb_writereg
        dut.wb_writedata.value = test_ut["In"].wb_writedata
        await ClockCycles(dut.clk, 2)
        test_all(dut, test_ut["Out"])
        dut._log.info("✔")

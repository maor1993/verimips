from dataclasses import dataclass
from Testing.RTL.MIPS.mips_consts import AluOps, Btype, Funct, MemOp, Opcodes, RegImm


@dataclass
class ControlInput:
    opcode: int = 0
    funct: int = 0
    reg_imm_type: int = 0


@dataclass
class ControlOutput:
    jump: bool = 0
    link: bool = 0
    branch: bool = 0
    branch_type: int = 0
    reg_write: bool = 0
    reg_dst: bool = 0
    reg_imm: bool = 0
    aluop: int = 0
    alusrc: bool = 0
    mem_write: bool = 0
    mem_read: bool = 0
    mem_toreg: bool = 0
    mem_unsigned: bool = 0
    mem_op: int = 0
    imm_unsigned: bool = 0
    invalid_inst: bool = 0
    cp0_reg_write: bool = 0
    cp0_reg_read: bool = 0
    cp0_subop: bool = 0


class ControlMaps:
    i_opcodes = {
        "addi": {"In": ControlInput(opcode=Opcodes.op_addi), "Out": ControlOutput(reg_write=1, alusrc=1, aluop=AluOps.aluop_add)},
        "addiu": {"In": ControlInput(opcode=Opcodes.op_addiu), "Out": ControlOutput(reg_write=1, alusrc=1, imm_unsigned=1, aluop=AluOps.aluop_addu)},
        "lui": {"In": ControlInput(opcode=Opcodes.op_lui), "Out": ControlOutput(reg_write=1, alusrc=1, aluop=AluOps.aluop_lui)},
        "andi": {"In": ControlInput(opcode=Opcodes.op_andi), "Out": ControlOutput(reg_write=1, alusrc=1, imm_unsigned=1, aluop=AluOps.aluop_and)},
        "ori": {"In": ControlInput(opcode=Opcodes.op_ori), "Out": ControlOutput(reg_write=1, alusrc=1, imm_unsigned=1, aluop=AluOps.aluop_or)},
        "slti": {"In": ControlInput(opcode=Opcodes.op_slti), "Out": ControlOutput(reg_write=1, alusrc=1, aluop=AluOps.aluop_slt)},
        "sltiu": {"In": ControlInput(opcode=Opcodes.op_sltiu), "Out": ControlOutput(reg_write=1, alusrc=1, aluop=AluOps.aluop_sltu)},
        "xori": {"In": ControlInput(opcode=Opcodes.op_xori), "Out": ControlOutput(reg_write=1, alusrc=1, imm_unsigned=1, aluop=AluOps.aluop_xor)},
        "beq": {"In": ControlInput(opcode=Opcodes.op_beq), "Out": ControlOutput(branch=1, branch_type=Btype.btype_beq, aluop=AluOps.aluop_sub)},
        "bne": {"In": ControlInput(opcode=Opcodes.op_bne), "Out": ControlOutput(branch=1, branch_type=Btype.btype_bne, aluop=AluOps.aluop_sub)},
        "blez": {"In": ControlInput(opcode=Opcodes.op_blez), "Out": ControlOutput(branch=1, branch_type=Btype.btype_blez, aluop=AluOps.aluop_slte)},
        "bgtz": {"In": ControlInput(opcode=Opcodes.op_bgtz), "Out": ControlOutput(branch=1, branch_type=Btype.btype_bgtz, aluop=AluOps.aluop_slte)},
        # "bgez": {"In": ControlInput(opcode=Opcodes.op_bge), "Out": ControlOutput(reg_write=1, alusrc=1, aluop=AluOps.aluop_sltu)},
        # "bltz": {"In": ControlInput(opcode=Opcodes.op_blt), "Out": ControlOutput(reg_write=1, alusrc=1, aluop=AluOps.aluop_sltu)},

        "lb": {"In": ControlInput(opcode=Opcodes.op_lb), "Out": ControlOutput(reg_write=1, alusrc=1, mem_read=1, mem_toreg=1,
                                                                              mem_op=MemOp.memop_byte, aluop=AluOps.aluop_add)},
        "lbu": {"In": ControlInput(opcode=Opcodes.op_lbu), "Out": ControlOutput(reg_write=1, alusrc=1, mem_read=1, mem_toreg=1,
                                                                                mem_op=MemOp.memop_byte, mem_unsigned=1, aluop=AluOps.aluop_add)},
        "lh": {"In": ControlInput(opcode=Opcodes.op_lh), "Out": ControlOutput(reg_write=1, alusrc=1, mem_read=1, mem_toreg=1,
                                                                              mem_op=MemOp.memop_half, aluop=AluOps.aluop_add)},
        "lhu": {"In": ControlInput(opcode=Opcodes.op_lhu), "Out": ControlOutput(reg_write=1, alusrc=1, mem_read=1, mem_toreg=1,
                                                                                mem_op=MemOp.memop_half, mem_unsigned=1, aluop=AluOps.aluop_add)},
        "lw": {"In": ControlInput(opcode=Opcodes.op_lw), "Out": ControlOutput(reg_write=1, alusrc=1, mem_read=1, mem_toreg=1,
                                                                              mem_op=MemOp.memop_word, aluop=AluOps.aluop_add)},



        "sb": {"In": ControlInput(opcode=Opcodes.op_sb), "Out": ControlOutput(mem_write=1, alusrc=1, mem_op=MemOp.memop_byte, aluop=AluOps.aluop_add)},
        "sh": {"In": ControlInput(opcode=Opcodes.op_sh), "Out": ControlOutput(mem_write=1, alusrc=1, mem_op=MemOp.memop_half, aluop=AluOps.aluop_add)},
        "sw": {"In": ControlInput(opcode=Opcodes.op_sw), "Out": ControlOutput(mem_write=1, alusrc=1, mem_op=MemOp.memop_word, aluop=AluOps.aluop_add)},
    }
    j_opcodes = {
        "j": {"In": ControlInput(opcode=Opcodes.op_j), "Out": ControlOutput(jump=1)},
        "jal": {"In": ControlInput(opcode=Opcodes.op_jal), "Out": ControlOutput(jump=1, link=1, reg_write=1)},


    }
    r_opcodes = {
        "bgez": {"In": ControlInput(opcode=Opcodes.op_regimm, reg_imm_type=RegImm.regimm_bgez), "Out": ControlOutput(branch=1, reg_imm=1,
                                                                                                                     aluop=AluOps.aluop_slt,
                                                                                                                     branch_type=Btype.btype_bgez)},
        "bgezal": {"In": ControlInput(opcode=Opcodes.op_regimm, reg_imm_type=RegImm.regimm_bgezal), "Out": ControlOutput(branch=1, link=1, reg_write=1,
                                                                                                                         reg_imm=1, aluop=AluOps.aluop_slt,
                                                                                                                         branch_type=Btype.btype_bgez)},
        "bltz": {"In": ControlInput(opcode=Opcodes.op_regimm, reg_imm_type=RegImm.regimm_bltz), "Out": ControlOutput(branch=1, reg_imm=1,
                                                                                                                     aluop=AluOps.aluop_slt,
                                                                                                                     branch_type=Btype.btype_bltz)},
        "bltzal": {"In": ControlInput(opcode=Opcodes.op_regimm, reg_imm_type=RegImm.regimm_bltzal), "Out": ControlOutput(branch=1, link=1, reg_write=1,
                                                                                                                         reg_imm=1, aluop=AluOps.aluop_slt,
                                                                                                                         branch_type=Btype.btype_bltz)}
    }
    special_opcodes = {
        "add": {"In": ControlInput(funct=Funct.funct_add), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},
        "addu": {"In": ControlInput(funct=Funct.funct_addu), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},
        "sub": {"In": ControlInput(funct=Funct.funct_sub), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},
        "subu": {"In": ControlInput(funct=Funct.funct_subu), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},

        "and": {"In": ControlInput(funct=Funct.funct_add), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},
        "nor": {"In": ControlInput(funct=Funct.funct_nor), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},
        "or": {"In": ControlInput(funct=Funct.funct_or), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},
        "xor": {"In": ControlInput(funct=Funct.funct_xor), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},

        "sll": {"In": ControlInput(funct=Funct.funct_sll), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},
        "sllv": {"In": ControlInput(funct=Funct.funct_sllv), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},
        "sra": {"In": ControlInput(funct=Funct.funct_sra), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},
        "srav": {"In": ControlInput(funct=Funct.funct_srav), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},
        "srl": {"In": ControlInput(funct=Funct.funct_srl), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},
        "srlv": {"In": ControlInput(funct=Funct.funct_srlv), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},

        "slt": {"In": ControlInput(funct=Funct.funct_slt), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},
        "sltu": {"In": ControlInput(funct=Funct.funct_sltu), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},

        # "break": {"In": ControlInput(funct=Funct.funct_jr), "Out": ControlOutput(jump=1, reg_dst=1)},
        # "syscall": {"In": ControlInput(funct=Funct.funct_jr), "Out": ControlOutput(jump=1, reg_dst=1)},

        "jr": {"In": ControlInput(funct=Funct.funct_jr), "Out": ControlOutput(jump=1, reg_dst=1)},
        "jalr": {"In": ControlInput(funct=Funct.funct_jalr), "Out": ControlOutput(jump=1, link=1, reg_write=1, reg_dst=1)},

        "mult": {"In": ControlInput(funct=Funct.funct_mult), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},
        "multu": {"In": ControlInput(funct=Funct.funct_multu), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},
        "mfhi": {"In": ControlInput(funct=Funct.funct_mfhi), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},
        "mflo": {"In": ControlInput(funct=Funct.funct_mflo), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},
        "mthi": {"In": ControlInput(funct=Funct.funct_mthi), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)},
        "mtlo": {"In": ControlInput(funct=Funct.funct_mtlo), "Out": ControlOutput(reg_write=1, reg_dst=1, aluop=AluOps.aluop_special)}
    }

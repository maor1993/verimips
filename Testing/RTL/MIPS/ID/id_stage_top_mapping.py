from dataclasses import dataclass
from Testing.RTL.MIPS.mips_consts import AluOps, MemOp, Opcodes, Funct, t_control_to_ex, t_control_to_m, t_control_to_wb


@dataclass
class IDStageTopInput:
    if_instruction: int = 0

    ex_memread: bool = 0
    ex_rt_in: int = 0

    mem_branch: bool = 0

    wb_regwrite: bool = 0
    wb_writereg: int = 0
    wb_writedata: int = 0


@dataclass
class IDStageTopOutput:
    ex_imm: int = 0
    ex_rs: int = 0
    ex_rt: int = 0
    ex_rd: int = 0
    ex_control_to_ex: t_control_to_ex = t_control_to_ex()
    ex_control_to_m: t_control_to_m = t_control_to_m()
    ex_control_to_wb: t_control_to_wb = t_control_to_wb()


def build_i_instruction(opcode: Opcodes, rs, rt, imm):
    return (opcode << 26) | (rs << 21) | (rt << 16) | imm


def build_j_instruction(opcode: Opcodes, imm):
    return (opcode << 26) | imm


def build_sepcial_instruction(funct: Funct, rs, rt, rd):
    return (rs << 21) | (rt << 16) | (rd << 11) | funct


@dataclass
class IdStageTopMaps:
    one_clock_tests = {
        "add_basic": {"In": IDStageTopInput(
            if_instruction=build_i_instruction(Opcodes.op_addi, 13, 22, 0x8000),
        ), "Out": IDStageTopOutput(
            ex_imm=0xffff8000,
            ex_rs=13,
            ex_rt=22,
            ex_control_to_ex=t_control_to_ex(aluop=AluOps.aluop_add, alusrc=1),
            ex_control_to_wb=t_control_to_wb(regwrite=1)
        )},
        "add_unsigned": {"In": IDStageTopInput(
            if_instruction=build_i_instruction(Opcodes.op_addiu, 23, 26, 0x8000),
        ), "Out": IDStageTopOutput(
            ex_imm=0x00008000,
            ex_rs=23,
            ex_rt=26,
            ex_control_to_ex=t_control_to_ex(aluop=AluOps.aluop_addu, alusrc=1),
            ex_control_to_wb=t_control_to_wb(regwrite=1)
        )},
        "sub_basic": {"In": IDStageTopInput(
            if_instruction=build_sepcial_instruction(Funct.funct_sub, 7, 0, 12),
        ), "Out": IDStageTopOutput(
            ex_rs=7,
            ex_rt=0,
            ex_rd=12,
            ex_control_to_ex=t_control_to_ex(aluop=AluOps.aluop_special, regdst=1),
            ex_control_to_wb=t_control_to_wb(regwrite=1)
        )},
        "jump_and_link": {"In": IDStageTopInput(
            if_instruction=build_j_instruction(Opcodes.op_jal, 0x1234)
        ), "Out": IDStageTopOutput(
            ex_imm=0x1234,
            ex_control_to_ex=t_control_to_ex(jump=1, link=1),
            ex_control_to_wb=t_control_to_wb(regwrite=1)
        )
        },
        "store_word": {"In": IDStageTopInput(
            if_instruction=build_i_instruction(Opcodes.op_sw, 13, 22, 0x8000)),
            "Out": IDStageTopOutput(
            ex_imm=0xFFFF8000,
            ex_rs=13,
            ex_rt=22,
            ex_control_to_ex=t_control_to_ex(memop=MemOp.memop_word, aluop=AluOps.aluop_add, alusrc=1),
            ex_control_to_m=t_control_to_m(memwrite=1)
        )},
        "load_word": {"In": IDStageTopInput(if_instruction=build_i_instruction(Opcodes.op_lw, 13, 22, 0x8000)),
                      "Out": IDStageTopOutput(
            ex_imm=0xFFFF8000,
            ex_rs=13,
            ex_rt=22,
            ex_control_to_ex=t_control_to_ex(memop=MemOp.memop_word, aluop=AluOps.aluop_add, alusrc=1),
            ex_control_to_m=t_control_to_m(memread=1),
            ex_control_to_wb=t_control_to_wb(regwrite=1, memtoreg=1)
        )},
    }

    hazzard_tests = {
        "data_hazzard_1": {"In": IDStageTopInput(if_instruction=build_i_instruction(Opcodes.op_addi, 13, 22, 0x8000),
                                                 ex_rt_in=13
                                                 )}
    }

from cocotb_test.simulator import run


def test_control_block():
    run(verilog_sources=["RTL/MIPS/ID/control.sv"],
        toplevel="control",
        module="control_tb",
        compile_args=["-grelative-include"])


def test_id_stage_top():
    run(verilog_sources=["RTL/MIPS/ID/id_stage_top.sv"],
        toplevel="id_stage_top",
        module="id_stage_top_tb",
        compile_args=["-grelative-include"])

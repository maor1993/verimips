from cocotb.triggers import Timer
import cocotb
from control_mapping import ControlMaps, ControlInput, ControlOutput
from Testing.RTL.tb_utils import update_test_number


def control_test_all(dut, expected: ControlOutput):
    assert dut.reg_dst.value == expected.reg_dst, f"reg_dst is incorrect, expected {expected.reg_dst}"
    assert dut.reg_imm.value == expected.reg_imm, f"reg_imm is incorrect, expected {expected.reg_imm}"
    assert dut.jump.value == expected.jump, f"jump is incorrect, expected {expected.jump}"
    assert dut.link.value == expected.link, f"link is incorrect, expected {expected.link}"
    assert dut.branch.value == expected.branch, f"branch is incorrect, expected {expected.branch}"
    assert dut.branch_type.value == expected.branch_type, f"branch_type is incorrect, expected {expected.branch_type}"
    assert dut.reg_write.value == expected.reg_write, f"reg_write is incorrect, expected {expected.reg_write}"
    assert dut.aluop.value == expected.aluop, f"aluop is incorrect, expected {expected.aluop}"
    assert dut.alusrc.value == expected.alusrc, f"alu_src is incorrect, expected {expected.alusrc}"
    assert dut.mem_write.value == expected.mem_write, f"mem_write is incorrect, expected {expected.mem_write}"
    assert dut.mem_read.value == expected.mem_read, f"mem_read is incorrect, expected {expected.mem_read}"
    assert dut.mem_toreg.value == expected.mem_toreg, f"mem_toreg is incorrect, expected {expected.mem_toreg}"
    assert dut.mem_unsigned.value == expected.mem_unsigned, f"mem_unsigned is incorrect, expected {expected.mem_unsigned}"
    assert dut.mem_op.value == expected.mem_op, f"mem_op is incorrect, expected {expected.mem_op}"
    assert dut.imm_unsigned.value == expected.imm_unsigned, f"imm_unsigned is incorrect, expected {expected.imm_unsigned}"
    assert dut.invalid_inst.value == expected.invalid_inst, f"invlaid_inst is incorrect, expected {expected.invalid_inst}"
    assert dut.cp0_reg_write.value == expected.cp0_reg_write, f"cp0_reg_write is incorrect, expected {expected.cp0_reg_write}"
    assert dut.cp0_reg_read.value == expected.cp0_reg_read, f"cp_reg_read is incorrect, expected {expected.cp0_reg_read}"
    assert dut.cp0_subop.value == expected.cp0_subop, f"cp0_subop is incorrect, expected {expected.cp0_subop}"


@cocotb.test()
async def i_opcodes(dut):
    dut._log.info("Case 1.1:Checking I Opcodes")
    update_test_number(dut,1)
    dut.rstn.value = 0
    await Timer(100, units='ns')
    dut.rstn.value = 1
    for idx, key in enumerate(ControlMaps.i_opcodes.keys()):
        opcode_ut: ControlInput = ControlMaps.i_opcodes[key]
        dut._log.info(f"running stimulus for opcode {key}")
        dut.op.value = opcode_ut["In"].opcode
        dut.funct.value = opcode_ut["In"].funct
        dut.reg_imm_type.value = opcode_ut["In"].reg_imm_type
        await Timer(10, units='ns')
        control_test_all(dut, opcode_ut["Out"])
        dut._log.info("✔")
        await Timer(10, units='ns')


@cocotb.test()
async def j_opcodes(dut):
    dut._log.info("Case 2.1:Checking J Opcodes")
    update_test_number(dut,2)
    dut.rstn.value = 0
    await Timer(100, units='ns')
    dut.rstn.value = 1
    for idx, key in enumerate(ControlMaps.j_opcodes.keys()):
        opcode_ut: ControlInput = ControlMaps.j_opcodes[key]
        dut._log.info(f"running stimulus for opcode {key}")
        dut.op.value = opcode_ut["In"].opcode
        dut.funct.value = opcode_ut["In"].funct
        dut.reg_imm_type.value = opcode_ut["In"].reg_imm_type
        await Timer(10, units='ns')
        control_test_all(dut, opcode_ut["Out"])
        dut._log.info("✔")
        await Timer(10, units='ns')


@cocotb.test()
async def r_opcodes(dut):
    dut._log.info("Case 3.1:Checking R opcodes")
    update_test_number(dut,3)
    dut.rstn.value = 0
    await Timer(100, units='ns')
    dut.rstn.value = 1
    for idx, key in enumerate(ControlMaps.r_opcodes.keys()):
        opcode_ut: ControlInput = ControlMaps.r_opcodes[key]
        dut._log.info(f"running stimulus for opcode {key}")
        dut.op.value = opcode_ut["In"].opcode
        dut.funct.value = opcode_ut["In"].funct
        dut.reg_imm_type.value = opcode_ut["In"].reg_imm_type
        await Timer(10, units='ns')
        control_test_all(dut, opcode_ut["Out"])
        dut._log.info("✔")
        await Timer(10, units='ns')


@cocotb.test()
async def special_opcodes(dut):
    dut._log.info("Case 4.1:Checking Special opcodes")
    update_test_number(dut,4)
    dut.rstn.value = 0
    await Timer(100, units='ns')
    dut.rstn.value = 1
    for idx, key in enumerate(ControlMaps.special_opcodes.keys()):
        opcode_ut: ControlInput = ControlMaps.special_opcodes[key]
        dut._log.info(f"running stimulus for opcode {key}")
        dut.op.value = opcode_ut["In"].opcode
        dut.funct.value = opcode_ut["In"].funct
        dut.reg_imm_type.value = opcode_ut["In"].reg_imm_type
        await Timer(10, units='ns')
        control_test_all(dut, opcode_ut["Out"])
        dut._log.info("✔")
        await Timer(10, units='ns')

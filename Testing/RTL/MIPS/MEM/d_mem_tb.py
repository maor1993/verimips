from cocotb.triggers import ClockCycles
from cocotb.clock import Clock
import cocotb
import numpy as np
from numpy.random import default_rng

from Testing.RTL.tb_utils import wait_for_debugger  # noqa

np.random.seed(0)


@cocotb.test()
async def write_then_read(dut):
    # Create a 10ns period clock on port clk
    clock = Clock(dut.clk, 10, units="ns")
    await cocotb.start(clock.start())
    # set all values to default while we are at it
    dut.addr.value = 0
    dut.byteen.value = 0
    dut.data.value = 0
    dut.rden.value = 0
    dut.wren.value = 0
    await ClockCycles(dut.clk, 1)

    # test case 1.1
    # write to memory and test internally that value was written
    randdata = np.random.randint(0, (2**32)-1)
    randaddr = np.random.randint(0, (2**12)-1)
    dut._log.info(f"Starting test case 1.1 with randaddr: {randaddr} and randdata: {randdata}")
    dut.data.value = randdata
    dut.addr.value = randaddr
    dut.byteen.value = 0xf
    await ClockCycles(dut.clk, 1)
    dut.wren.value = 1
    await ClockCycles(dut.clk, 1)
    dut.wren.value = 0
    await ClockCycles(dut.clk, 1)

    assert dut.mem[randaddr].value == randdata, f"Expected 0x{randdata:x} but got {hex(dut.mem[randaddr].value)}"

    # test case 1.2
    # attempt to read the same address and see if output data is correct
    dut._log.info("Starting test case 1.2")
    dut.rden.value = 1
    await ClockCycles(dut.clk, 1)
    dut.rden.value = 0
    await ClockCycles(dut.clk, 1)

    assert dut.q.value == randdata, f"Expected 0x{randdata:x} but got {hex(dut.q.value)}"


@cocotb.test()
async def highspeed_read(dut):
    TEST_SIZE = 1000
    # Create a 10ns period clock on port clk
    clock = Clock(dut.clk, 10, units="ns")
    await cocotb.start(clock.start())
    # set all values to default while we are at it
    dut.addr.value = 0
    dut.byteen.value = 0
    dut.data.value = 0
    dut.rden.value = 0
    dut.wren.value = 0
    await ClockCycles(dut.clk, 1)

    # test case 2.1
    # write a large amount of random data and attempt to read back
    dut._log.info(f"Starting test case 2.1 with test size of {TEST_SIZE}")

    # generate random list of unique addrs and data to write
    rng = default_rng()
    randaddr = rng.choice(np.arange(2**12), size=TEST_SIZE, replace=False)
    randdata = np.random.randint(0, (2**32)-1, TEST_SIZE)

    # write everything to the ram
    for addr, data in zip(randaddr, randdata):
        dut.data.value = int(data)
        dut.addr.value = int(addr)
        dut.byteen.value = 0xf
        await ClockCycles(dut.clk, 1)
        dut.wren.value = 1
        await ClockCycles(dut.clk, 1)
        dut.wren.value = 0
        await ClockCycles(dut.clk, 1)

        # validate inside ram that data was written correctly
        assert dut.mem[addr].value == data, f"Expected 0x{data:x} but got {hex(dut.mem[addr].value)}"
        dut._log.debug(f"addr: 0x{addr:X} data: 0x{data:x} OK")

    # test case 2.2
    # read back all data and validate
    dut._log.info("Starting test case 2.2")
    dut.rden.value = 1
    await ClockCycles(dut.clk, 1)
    for addr, data in zip(randaddr, randdata):
        dut.addr.value = int(addr)
        await ClockCycles(dut.clk, 1)
        await ClockCycles(dut.clk, 1)
        assert dut.q.value == data, f"Expected 0x{data:x} but got {hex(dut.q.value)}"
        dut._log.debug(f"addr: 0x{addr:X} data: 0x{data:x} OK")


@cocotb.test()
async def write_then_read_with_byteen(dut):
    # Create a 10ns period clock on port clk
    clock = Clock(dut.clk, 10, units="ns")
    await cocotb.start(clock.start())
    # set all values to default while we are at it
    dut.addr.value = 0
    dut.byteen.value = 0
    dut.data.value = 0
    dut.rden.value = 0
    dut.wren.value = 0
    await ClockCycles(dut.clk, 1)

    # test case 3.1
    # write to memory and test internally that value was written
    randdata = np.random.randint(0, (2**32)-1)
    randaddr = np.random.randint(0, (2**12)-1)
    dut._log.info(f"Starting test case 3.1 with randaddr: {randaddr} and randdata: {randdata}")
    dut.data.value = randdata
    dut.addr.value = randaddr
    dut.byteen.value = 0xf
    await ClockCycles(dut.clk, 1)
    dut.wren.value = 1
    await ClockCycles(dut.clk, 1)
    dut.wren.value = 0
    await ClockCycles(dut.clk, 1)

    assert dut.mem[randaddr].value == randdata, f"Expected 0x{randdata:x} but got {hex(dut.mem[randaddr].value)}"

    # now, lets write only the first two byte and esure that only they changed
    dut._log.info("Starting test case 3.2: word write")
    dut.byteen.value = 0x3
    dut.data.value = 0
    await ClockCycles(dut.clk, 1)
    dut.wren.value = 1
    await ClockCycles(dut.clk, 1)
    dut.wren.value = 0
    await ClockCycles(dut.clk, 1)

    assert dut.mem[randaddr].value == randdata & 0xffff0000, f"Expected 0x{randdata&0xffff0000:x} but got {hex(dut.mem[randaddr].value)}"

    # now lets write only one byte and esure that only that changed
    dut._log.info("Starting test case 3.3: byte write")
    dut.byteen.value = 0x1
    dut.data.value = 0x12345678
    dut.wren.value = 1
    await ClockCycles(dut.clk, 1)
    dut.wren.value = 0
    await ClockCycles(dut.clk, 1)

    expectedval = randdata & 0xffff0000 | 0x78
    assert dut.mem[randaddr].value == expectedval, f"Expected 0x{expectedval:x} but got {hex(dut.mem[randaddr].value)}"

from cocotb_test.simulator import run


def test_d_mem():
    run(verilog_sources=["RTL/MIPS/MEM/d_mem.sv"],
        toplevel="d_mem",
        module="d_mem_tb",
        compile_args=["-grelative-include"])


if __name__ == "__main__":
    test_d_mem()

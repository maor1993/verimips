import debugpy
from cocotb.triggers import RisingEdge, Timer


def wait_for_debugger():
    debugpy.listen(('localhost', 5678))
    debugpy.wait_for_client()
    debugpy.breakpoint()


async def wait_clocks(clk, clocks):
    for _ in range(clocks):
        await RisingEdge(clk)


async def immidate_assign(signal, val):
    signal.value = val
    await Timer(0, 'ns')


def update_test_number(dut, test_number):
    try:
        dut.test_number.value = test_number
    except AttributeError:
        dut._log.warn("test_number not found in dut")
        pass

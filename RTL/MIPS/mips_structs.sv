`ifndef MIPS_STRUCTS_SV
`define MIPS_STRUCTS_SV
typedef struct packed {
    logic memunsigned;
    logic memwrite;
    logic memread;
    logic cp0_reg_read;
    logic branch;
  } t_control_to_m;

  typedef struct packed {
    logic link;
    logic jump;
    logic [3:0] aluop;
    logic alusrc;
    logic regdst;
    logic [1:0] memop;
    logic [2:0] branchtype;
    logic cp0_reg_write;
  } t_control_to_ex;

  typedef struct packed {
    logic memtoreg;
    logic regwrite;
  } t_control_to_wb;

  typedef enum {
    SPECIAL_OPCODE = 0,
    I_OPCODE = 1,
    J_OPCODE = 2,
    R_OPCODE = 3
  } e_opcodes;
  typedef enum {  
    NO_FORWARD = 2'b00,
    FORWARD_WB = 2'b01,
    FORWARD_MEM= 2'b10,
    FORWARD_ERR= 2'b11
  } forward_command;

`endif

`timescale 1ns / 1ns
module d_mem #(
    parameter ADDR_WIDTH = 12,
    parameter DATA_WIDTH = 32
) (
    input [ADDR_WIDTH-1:0] addr,
    input [(DATA_WIDTH/8)-1:0] byteen,
    input [DATA_WIDTH-1:0] data,
    input rden,
    input wren,
    input clk,
    output reg [DATA_WIDTH-1:0] q
);


  reg [DATA_WIDTH-1:0] mem[2**ADDR_WIDTH];

  always @(posedge clk) begin : read_proc
    if (rden) begin
      q <= mem[addr];
    end
  end
  always @(posedge clk) begin : write_proc
    if (wren) begin
      //FIXME: only supports 32 bitdata widths :(      
      if (byteen[0]) begin
        mem[addr][7:0] <= data[7:0];
      end
      if (byteen[1]) begin
        mem[addr][15:8] <= data[15:8];
      end
      if (byteen[2]) begin
        mem[addr][23:16] <= data[23:16];
      end
      if (byteen[3]) begin
        mem[addr][31:24] <= data[31:24];
      end
    end
  end
endmodule

`include "../mips_consts.sv"
module branch_control (
    input      [2:0] branch_type, //! branch event we're hadnling
    input            jump, //! jump was triggered
    input            alu_zero, //! input from ALU
    output reg       zero_res //! ???
);

  import mips_consts::*;

  always @(branch_type or jump or alu_zero) begin
    if (jump) begin
      zero_res = 1;
    end else begin
      case (branch_type)
        c_btype_bne: zero_res = !alu_zero;
        c_btype_bltz: zero_res = !alu_zero;
        c_btype_blez: zero_res = !alu_zero;
        default: zero_res = alu_zero;
      endcase
    end
  end

endmodule

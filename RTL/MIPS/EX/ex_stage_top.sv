`include "../mips_structs.sv"
`include "alu.sv"
`include "alu_control.sv"
`include "branch_control.sv"
`include "forwarding_unit.sv"
`include "../adder.sv"
`timescale 1ns / 1ns
module ex_stage_top (
    input [31:0] id_next_pc,  //!from ID stage
    input [31:0] id_read_data1,  //!from ID stage
    input [31:0] id_read_data2,  //!from ID stage

    input [31:0] id_imm_ext,
    input [ 4:0] id_shamt,

    input t_control_to_ex id_control_to_ex,
    input t_control_to_m  id_control_to_m,
    input t_control_to_wb id_control_to_wb,

    input [4:0] id_rs,
    input [4:0] id_rt,
    input [4:0] id_rd,

    input [2:0] id_branchtype,


    //to id stage
    output reg id_memread,
    output reg id_cp0_read,
    output reg [4:0] id_rt_in,



    //to mem stage
    output mem_mem_write,
    output mem_mem_read,
    output mem_reg_write,
    output mem_mem_to_reg,
    output mem_zero,
    output mem_branch,

    output reg [31:0] mem_alu_res,
    output reg [31:0] mem_data,
    output reg [4:0] mem_rd,
    output reg [31:0] mem_target_addr,
    output reg [3:0] mem_byteen,
    output reg mem_unsigned,
    output reg mem_cp0_read,

    //from mem stage
    input [4:0] mem_to_ex_rd,
    input mem_to_ex_regwrite,
    input mem_to_ex_branch,
    input [31:0] mem_to_ex_alu_res,

    //from wb stage
    input [4:0] wb_to_ex_rd,
    input wb_to_ex_regwrite,
    input [31:0] wb_to_ex_wrtitedata,


    //cp0 connections
    output reg cp0_reg_write,
    output reg [31:0] cp0_data_out,
    output reg [4:0] cp0_rd,
    output reg [5:0] cp0_irqs,
    output reg [31:0] cp0_next_pc,


    //external connections 
    input [5:0] irq,

    input clk,
    input rstn
);

  wire [1:0] forward_a;
  wire [1:0] forward_b;

  reg [31:0] alu_in1;
  reg [31:0] alu_in2;
  reg [31:0] mux2_res;
  wire [4:0] alu_operation;
  wire alu_op_var;
  wire [31:0] alu_result;
  wire alu_zero;
  wire zero_out;
  wire [31:0] imm_shifted;
  wire [31:0] adder_target_addr;

  reg [31:0] wb_to_ex_wrtitedata_d;
  reg [31:0] mem_to_ex_alu_res_d;
  reg wb_to_ex_regwrite_d;
  reg [4:0] wb_to_ex_rd_d;

  wire [1:0] jumptgt_agg;
  wire [1:0] regdst_agg;
  reg branch_jump;

  reg [31:0] mem_target_addr_mux;
  reg [4:0] mem_rd_mux;

  alu_control alu_control_dut (
      .funct(id_imm_ext[5:0]),
      .aluop(id_control_to_ex.aluop),
      .operation(alu_operation),
      .op_var(alu_op_var),
      .rstn(rstn)
  );

  alu alu_dut (
      .in1(alu_in1),
      .in2(alu_in2),
      .shamt(id_shamt),
      .operation(alu_operation),
      .op_var(alu_op_var),
      .result(alu_result),
      .zero(alu_zero),
      .clk(clk),
      .rstn(rstn)
  );
  branch_control branch_control_dut (
      .branch_type(id_branchtype),
      .jump(id_control_to_ex.jump),
      .alu_zero(alu_zero),
      .zero_res(zero_out)
  );
  forwarding_unit forwarding_unit_dut (
      .forward_a(forward_a),
      .forward_b(forward_b),
      .id_rs(id_rs),
      .id_rt(id_rt),
      .mem_rd(mem_rd),
      .mem_regwrite(mem_to_ex_regwrite),
      .wb_rd(wb_to_ex_rd_d),
      .wb_regwrite(wb_to_ex_regwrite_d),
      .rstn(rstn)
  );
  adder adder_dut (
      .x(id_next_pc),
      .y(imm_shifted),
      .z(adder_target_addr)
  );

  always @(posedge clk or rstn) begin : forward_dly_proc
    if (!rstn) begin
      wb_to_ex_rd_d = 0;
      wb_to_ex_regwrite_d = 0;
      mem_to_ex_alu_res_d = 0;
      wb_to_ex_wrtitedata_d = 0;
    end else begin
      wb_to_ex_rd_d = wb_to_ex_rd;
      wb_to_ex_regwrite_d = wb_to_ex_regwrite;
      mem_to_ex_alu_res_d = mem_to_ex_alu_res;
      wb_to_ex_wrtitedata_d = wb_to_ex_wrtitedata;
    end
  end

  always @(posedge clk ) begin: ex_mem_dff_proc
      if (!rstn | mem_to_ex_branch) begin
          mem_reg_write = 0;
          mem_mem_to_reg=0;
          mem_branch=0;
          mem_target_addr=0;
          mem_zero=0;
          mem_rd=0;
          mem_unsigned=0;
          mem_cp0_read=0;
          cp0_irqs = 6'b111111;
      end 
      else begin
          //buffer out all the signals
        mem_reg_write = id_control_to_wb.regwrite;
        mem_mem_to_reg=id_control_to_wb.memtoreg;
        mem_branch= branch_jump;
        mem_target_addr = mem_target_addr_mux;
        mem_zero = zero_out;
        mem_rd = mem_rd_mux;
        mem_unsigned = id_control_to_m.memunsigned;
        mem_cp0_read = id_control_to_m.cp0_reg_read;
        cp0_irqs = irq;
      end
  end   

  assign imm_shifted = {id_imm_ext[29:0], 2'b0};

  //forwarding unit mapping
  assign alu_in1 = s_forward==NO_FORWARD? id_read_data1: s_forward==FORWARD_MEM? mem_to_ex_alu_res_d: s_forward==FORWARD_WB? wb_to_ex_wrtitedata_d: 0;
  assign mux2_res = s_forward==NO_FORWARD? id_read_data2: s_forward==FORWARD_MEM? mem_to_ex_alu_res_d: s_forward==FORWARD_WB? wb_to_ex_wrtitedata_d: 0;
  assign alu_in2 = id_control_to_ex.alusrc ? id_imm_ext : mux2_res;

  //jump instructions
  assign regdst_agg = {id_control_to_ex.link, id_control_to_ex.regdst};
  assign jumptgt_agg = {id_control_to_ex.jump, id_control_to_ex.regdst};

  //cp0 connecitons
  assign cp0_reg_write = id_control_to_ex.cp0_reg_write;
  assign cp0_rd = id_control_to_ex.cp0_rd;
  assign cp0_data_out = mux2_res;
  assign cp0_next_pc =  branch_jump? mem_target_addr : id_next_pc;

  assign branch_jump = id_control_to_ex.jump|id_control_to_ex.branch;


endmodule

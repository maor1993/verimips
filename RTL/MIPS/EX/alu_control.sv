`include "../mips_consts.sv"

module alu_control (
    input [5:0] funct,  //!special operation from opcode
    input [3:0] aluop,  //!operation defined from control
    output reg [4:0] operation,  //!actual operation sent to alu
    output reg op_var,  //!handles LUI, variable shifts
    input rstn  //!reset
);

  import mips_consts::*;

  always @(aluop  or  funct) begin: alu_mux
    op_var = 0;
    case (aluop)
      c_aluop_special: begin
        case (funct)
          c_funct_add: operation = c_alu_add;
          c_funct_addu: operation = c_alu_addu;
          c_funct_sub: operation = c_alu_sub;
          c_funct_subu: operation = c_alu_subu;
          c_funct_and: operation = c_alu_and;
          c_funct_or: operation = c_alu_or;
          c_funct_xor: operation = c_alu_xor;
          c_funct_nor: operation = c_alu_nor;
          c_funct_sll: operation = c_alu_sll;
          c_funct_srl: operation = c_alu_srl;
          c_funct_sra: operation = c_alu_sra;
          c_funct_slt: operation = c_alu_slt;
          c_funct_sltu: operation = c_alu_sltu;
          c_funct_mult: operation = c_alu_mult;
          c_funct_multu: operation = c_alu_multu;
          c_funct_mfhi: operation = c_alu_mfhi;
          c_funct_mflo: operation = c_alu_mflo;
          c_funct_sllv: begin
            operation = c_alu_sll;
            op_var = 1;
          end
          c_funct_srlv: begin
            operation = c_alu_srl;
            op_var = 1;
          end
          c_funct_srav: begin
            operation = c_alu_sra;
            op_var = 1;
          end
          default: operation = c_alu_nop;  //unless otherwise metioned, all other functs are nop

        endcase

      end
      c_aluop_add: operation = c_alu_add;
      c_aluop_addu: operation = c_alu_addu;
      c_aluop_sub: operation = c_alu_sub;
      c_aluop_subu: operation = c_alu_subu;
      c_aluop_and: operation = c_alu_and;
      c_aluop_or: operation = c_alu_or;
      c_aluop_xor: operation = c_alu_xor;
      c_aluop_slt: operation = c_alu_slt;
      c_aluop_sltu: operation = c_alu_sltu;
      c_aluop_slte: operation = c_alu_slte;
      c_aluop_lui: begin
        operation = c_alu_addu;
        op_var = 1;
      end
      default: operation = c_alu_nop;  //unless otherwise metioned, all other aluops are nop
    endcase
  end





endmodule

`include "../mips_structs.sv"
module forwarding_unit (
    output reg [1:0] forward_a,
    output reg [1:0] forward_b,

    input [4:0] id_rs,
    input [4:0] id_rt,


    input [4:0] mem_rd,
    input mem_regwrite,

    input [4:0] wb_rd,
    input wb_regwrite,

    input rstn
);




  always @(rstn or id_rs or id_rt or mem_rd or wb_rd or id_rs or id_rt or mem_regwrite or wb_regwrite) begin
    if (!rstn) begin
      forward_a = NO_FORWARD;
      forward_b = NO_FORWARD;
    end else begin
      forward_a = NO_FORWARD;
      forward_b = NO_FORWARD;

      if ((id_rs == mem_rd) && (mem_rd != 0) && (mem_regwrite)) begin
        forward_a = FORWARD_MEM;
      end else if ((id_rs == wb_rd) && (wb_rd != 0) && (wb_regwrite)) begin
        forward_a = FORWARD_WB;
      end

      if ((id_rt == mem_rd) && (mem_rd != 0) && (mem_regwrite)) begin
        forward_b = FORWARD_MEM;
      end else if ((id_rt == wb_rd) && (wb_rd != 0) && (wb_regwrite)) begin
        forward_b = FORWARD_WB;
      end
    end

  end

endmodule

`include "../mips_consts.sv"
module alu (
    input [31:0] in1,
    input [31:0] in2,
    input [4:0] shamt,
    input [4:0] operation,
    input op_var,
    output reg [31:0] result,
    output reg zero,
    input clk,
    input rstn
);


  import mips_consts::*;

  reg [63:0] md_result;
  reg [31:0] hi_reg;
  reg [31:0] lo_reg;
  reg update_md;


  //multiplier buffer 
  always @(posedge clk or rstn) begin : muldiv_buffer
    if (!rstn) begin
      hi_reg <= 0;
      lo_reg <= 0;
    end else begin
      if (update_md) begin
        hi_reg <= md_result[63:32];
        lo_reg <= md_result[31:0];
      end
    end
  end


  always @(rstn or in1 or in2 or operation or hi_reg or lo_reg or shamt or op_var) begin: alu_execute
    if (!rstn) begin
      md_result = 0;
      update_md = 0;
      result = 0;
    end else begin
      md_result = 0;
      update_md = 0;
      result = 0;
      case (operation)
        c_alu_add: result = $signed(in1) + $signed(in2);
        c_alu_addu: begin
          if (op_var) result = $signed(in2) << 16;
          else result = $unsigned(in1) + $unsigned(in2);
        end
        c_alu_and: result = in1 & in2;
        c_alu_or: result = in1 | in2;
        c_alu_xor: result = in1 ^ in2;
        c_alu_nor: result = ~(in1 | in2);
        c_alu_sub: result = $signed(in1) - $signed(in2);
        c_alu_subu: result = $unsigned(in1) - $unsigned(in2);
        c_alu_slt: result = $signed(in1) < $signed(in2);
        c_alu_sltu: result = $unsigned(in1) < $unsigned(in2);
        c_alu_mult: md_result = $signed(in1) * $signed(in2);
        c_alu_multu: md_result = $unsigned(in1) * $unsigned(in2);
        c_alu_mfhi: result = hi_reg;
        c_alu_mflo: result = lo_reg;
        c_alu_sll: begin
          if (op_var) result = $unsigned(in2) << $unsigned(in1);
          else result = $unsigned(in2) << $unsigned(shamt);
        end
        c_alu_srl: begin
          if (op_var) result = $unsigned(in2) >> $unsigned(in1);
          else result = $unsigned(in2) >> $unsigned(shamt);
        end
        c_alu_sra: begin
          if (op_var) result = $signed(in2) >> $unsigned(in1);
          else result = $signed(in2) >> $unsigned(shamt);
        end
        c_alu_slte: result = $signed(in1) <= $signed(in2);
        default: ;
      endcase
    end
  end
  assign zero = (result == 0) ? 1'b1 : 1'b0;
endmodule

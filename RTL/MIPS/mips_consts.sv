`ifndef MIPS_CONSTS_SV
`define MIPS_CONSTS_SV
//! @title MIPS Consts package
//! @author Maor.m
//! @version 0.1
//! @date 2022
//! @purpose huge list of all opcodes running in the system
package mips_consts;

  //---------------opcodes-------------------------------

  //--basic
  parameter c_op_special = 6'b000000;
  parameter c_op_addi = 6'b001000;
  parameter c_op_addiu = 6'b001001;
  parameter c_op_andi = 6'b001100;
  parameter c_op_ori = 6'b001101;
  parameter c_op_slti = 6'b001010;
  parameter c_op_sltiu = 6'b001011;
  parameter c_op_xori = 6'b001110;
  // --branch jumps
  parameter c_op_regimm = 6'b000001;
  parameter c_op_beq = 6'b000100;
  parameter c_op_bne = 6'b000101;
  parameter c_op_bgtz = 6'b000111;
  parameter c_op_blez = 6'b000110;
  parameter c_op_j = 6'b000010;
  parameter c_op_jal = 6'b000011;


  // --mem i/f commands
  parameter c_op_lb = 6'b100000;
  parameter c_op_lbu = 6'b100100;
  parameter c_op_lh = 6'b100001;
  parameter c_op_lhu = 6'b100101;
  parameter c_op_lui = 6'b001111;
  parameter c_op_lw = 6'b100011;
  parameter c_op_lwl = 6'b100010;
  parameter c_op_lwr = 6'b100110;

  parameter c_op_sb = 6'b101000;
  parameter c_op_sh = 6'b101001;
  parameter c_op_sw = 6'b101011;
  parameter c_op_swl = 6'b101010;
  parameter c_op_swr = 6'b101110;
  // ---------------------------------------------------------


  // ---------------BranchJump Values------------------------
  parameter c_regimm_bgez = 5'b00001;
  parameter c_regimm_bgezal = 5'b10001;
  parameter c_regimm_bltz = 5'b00000;
  parameter c_regimm_bltzal = 5'b10000;
  // -------------------------------------------------------

  // --------------special subopcodes-----------------------
  parameter c_funct_add = 6'b100000;
  parameter c_funct_addu = 6'b100001;
  parameter c_funct_sub = 6'b100010;
  parameter c_funct_subu = 6'b100011;

  parameter c_funct_and = 6'b100100;
  parameter c_funct_nor = 6'b100111;
  parameter c_funct_or = 6'b100101;
  parameter c_funct_xor = 6'b100110;


  parameter c_funct_sll = 6'b000000;
  parameter c_funct_sllv = 6'b000100;
  parameter c_funct_sra = 6'b000011;
  parameter c_funct_srav = 6'b000111;
  parameter c_funct_srl = 6'b000010;
  parameter c_funct_srlv = 6'b000110;

  parameter c_funct_slt = 6'b101010;
  parameter c_funct_sltu = 6'b101011;

  parameter c_funct_break = 6'b001101;
  parameter c_funct_syscall = 6'b001100;

  parameter c_funct_jalr = 6'b001001;
  parameter c_funct_jr = 6'b001000;

  parameter c_funct_mult = 6'b011000;
  parameter c_funct_multu = 6'b011001;
  //parameter c_funct_div = 6'b011010; not implemented
  //parameter c_funct_divu = 6'b011011; not implemented
  parameter c_funct_mfhi = 6'b010000;
  parameter c_funct_mflo = 6'b010010;
  parameter c_funct_mthi = 6'b010001;
  parameter c_funct_mtlo = 6'b010011;

  //co processor access
  parameter c_op_cop0 = 6'b010000;


  // -------------------------------------------------------



  // ------------------ALUOP COMMANDS-----------------------
  parameter c_aluop_add = 4'b0000;
  parameter c_aluop_sub = 4'b0001;
  parameter c_aluop_special = 4'b0010;
  parameter c_aluop_addu = 4'b0011;
  parameter c_aluop_subu = 4'b0100;
  parameter c_aluop_and = 4'b0101;
  parameter c_aluop_or = 4'b0110;
  parameter c_aluop_xor = 4'b0111;
  parameter c_aluop_slt = 4'b1000;
  parameter c_aluop_sltu = 4'b1001;
  parameter c_aluop_lui = 4'b1010;
  parameter c_aluop_slte = 4'b1011;


  // ------------------ALU OPERATIONS-----------------------
  parameter c_alu_nop = 5'b00000;
  parameter c_alu_add = 5'b00001;
  parameter c_alu_addu = 5'b10001;
  parameter c_alu_sub = 5'b00010;
  parameter c_alu_subu = 5'b10010;
  parameter c_alu_nor = 5'b00100;
  parameter c_alu_xor = 5'b00101;
  parameter c_alu_slt = 5'b00110;
  parameter c_alu_sltu = 5'b10110;
  parameter c_alu_and = 5'b00111;
  parameter c_alu_or = 5'b01000;
  parameter c_alu_mult = 5'b11000;
  parameter c_alu_multu = 5'b11001;
  parameter c_alu_mfhi = 5'b11100;
  parameter c_alu_mflo = 5'b11101;
  parameter c_alu_sll = 5'b01010;
  parameter c_alu_srl = 5'b01011;
  parameter c_alu_sra = 5'b01001;
  parameter c_alu_slte = 5'b01110;


  // ------------------------------------------------------

  // --------------------memory sizes---------------------
  parameter c_memop_word = 2'b00;
  parameter c_memop_byte = 2'b01;
  parameter c_memop_half = 2'b10;
  // ----------------------------------------------------

  // --------------------Branch Types--------------------
  parameter c_btype_beq = 3'b000;  //opcode
  parameter c_btype_bne = 3'b001;  //opcode
  parameter c_btype_bgez = 3'b010;  //uses regimm
  parameter c_btype_bltz = 3'b011;  //uses regimm
  parameter c_btype_bgtz = 3'b100;  //opcode
  parameter c_btype_blez = 3'b101;  //opcode
  // ----------------------------------------------------




endpackage

`endif

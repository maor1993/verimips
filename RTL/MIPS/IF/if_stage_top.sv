//! @title MIPS IF Stage
//! @author Maor.m
//! @version 0.1
//! @date 2022



//! R type Opcode:
//! {reg:[
//!    {bits:6,name: 'funct'},
//!    {bits:5,name: 'shamt'},
//!    {bits:5,name: 'rd'},
//!    {bits:5,name: 'rt'},
//!    {bits:5,name: 'rs'},
//!    {bits:6,name: 'opcode',type:3},
//!]}

//! I type Opcode:
//! {reg:[
//!    {bits:16,name: 'IMM'},
//!    {bits:5,name: 'rt'},
//!    {bits:5,name: 'rs'},
//!    {bits:6,name: 'opcode',type:3},
//!]}

//! J type Opcode:
//! {reg:[
//!    {bits:26,name: 'Jump Addr'},
//!    {bits:6,name: 'opcode',type:3},
//!]}

`include "i_mem.sv"
`include "if_mux.sv"
`include "pc.sv"
`include "../adder.sv"
`timescale 1ns/1ns
module if_stage_top
  #(
     parameter RESET_ADDR = 32'h0000_0000
   )
   (
     output [31:0] id_instruction, //!the current instruction to decode
     output reg [31:0] id_next_pc, //!next program counter to pipe through
     output [4:0] id_read_reg1, //! Reg1 for this command
     output [4:0] id_read_reg2,//! Reg2 for this command
     input id_stall, //! ID has requested a stall
     input mem_branch, //! mem has triggered a branch
     input [31:0] mem_new_pc ,//! new pc to go due to branching
     input clk,
     input rstn
   );

`ifdef COCOTB_SIM
  initial
  begin
    $dumpfile ("if_stage_top.vcd");
    $dumpvars (0, if_stage_top);
    #1;
  end
  reg [31:0] test_number= 0;
`endif

  wire [31:0] curr_addr;
  wire [31:0] adder_addr;
  wire [31:0] load_addr;
  wire s_id_stalln;
  reg [11:0] i_mem_addr;
  reg i_mem_aclr;
  wire [31:0] instruction;
  wire pc_rst;

  i_mem
    #(
      .ADDR_WIDTH(12), //fixme: upstream...
      .DATA_WIDTH (32)
    )
    i_mem_dut (
      .addr (curr_addr[13:2]),
      .rden (s_id_stalln),
      .aclr (1'b0),
      .clk (clk ),
      .q  (instruction)
    );

  if_mux
    if_mux_dut (
      .next_addr (id_next_pc),
      .branch_addr (mem_new_pc),
      .branch (mem_branch),
      .new_addr  (load_addr)
    );

  pc
    #(.RESET_ADDR (RESET_ADDR))
    pc_dut (
      .load_addr (load_addr),
      .stalln (s_id_stalln),
      .current_addr (curr_addr),
      .clk (clk ),
      .rstn  (rstn)
    );

  adder
    adder_dut (
      .x (curr_addr),
      .y (32'h4),
      .z  (id_next_pc)
    );


  assign id_read_reg1 = instruction[25:21];
  assign id_read_reg2 = instruction[20:16];
  assign id_instruction = instruction;
  assign s_id_stalln = !id_stall;

endmodule

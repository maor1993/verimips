`timescale 1ns/1ns
//! @author Maor.m
//! @title PC
//! @version 0.1
//! @date 2022
// Program counter


module pc 
  #(parameter RESET_ADDR = 32'h0000_0000)
  (
    input [31:0] load_addr, //!new address to jump to
    input stalln, //!pauses the program counter
    output reg [31:0] current_addr, //! current 32 bit instruction address
    input clk,
    input rstn
  );



  always @(posedge clk or negedge rstn)
  begin
    if(!rstn)
    begin
      current_addr <= RESET_ADDR;
    end
    else
    begin
      if(stalln)
      begin
        current_addr <= load_addr;
      end
    end
  end

endmodule

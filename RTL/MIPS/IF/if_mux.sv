`timescale 1ns/1ns
//! @title IF MUX
//! @author Maor.m
//! @version 0.1
//! @date 2022
// selects which address to use


module if_mux (
    input [31:0] next_addr,
    input [31:0] branch_addr,
    input branch,
    output [31:0] new_addr
);
    
assign new_addr = branch == 1? branch_addr : next_addr; 

endmodule
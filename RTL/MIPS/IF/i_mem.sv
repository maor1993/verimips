`timescale 1ns/1ns
module i_mem
  #(parameter ADDR_WIDTH = 12,
    parameter DATA_WIDTH =32)
   (
     input [ADDR_WIDTH-1:0] addr,
     input rden,
     input aclr,
     input clk,
     output reg [DATA_WIDTH-1:0] q  
   );


  reg [DATA_WIDTH-1:0] mem [2**ADDR_WIDTH-1:0];

  always @(posedge clk)
  begin
    if (rden)
    begin
      q <= mem[addr];
    end 

  end
endmodule

`timescale 1ns/1ns
//! @title Adder
//! @author Maor.m
//! @version 0.1
//! @date 2022
//! Adds two inputs 

module adder (
    input [31:0] x,
    input [31:0] y,
    output[31:0] z
);
    
assign z=x+y;

endmodule




module mips_registers (
    input [4:0] read_reg1,
    input [4:0] read_reg2,
    input [4:0] write_reg,

    output [31:0] read_data1,
    output [31:0] read_data2,
    input [31:0] write_data,
    input write_en,

    input clk,
    input rstn
    
);

reg [31:0] reg_file [32];
reg [31:0] reg_write_data;

//writing to register

always @(posedge clk) begin
    if (!rstn) begin
        reg_file[0] = 0;
        reg_file[1] = 0;
        reg_file[2] = 0;
        reg_file[3] = 0;
        reg_file[4] = 0;
        reg_file[5] = 0;
        reg_file[6] = 0;
        reg_file[7] = 0;
        reg_file[8] = 0;
        reg_file[9] = 0;
        reg_file[10] = 0;
        reg_file[11] = 0;
        reg_file[12] = 0;
        reg_file[13] = 0;
        reg_file[14] = 0;
        reg_file[15] = 0;
        reg_file[16] = 0;
        reg_file[17] = 0;
        reg_file[18] = 0;
        reg_file[19] = 0;
        reg_file[20] = 0;
        reg_file[21] = 0;
        reg_file[22] = 0;
        reg_file[23] = 0;
        reg_file[24] = 0;
        reg_file[25] = 0;
        reg_file[26] = 0;
        reg_file[27] = 0;
        reg_file[28] = 0;
        reg_file[29] = 0;
        reg_file[30] = 0;
        reg_file[31] = 0;
    end
    else if (write_en) begin
        reg_file[write_reg] = reg_write_data;
    end
end




// reading from registers
assign read_data1 = reg_file[read_reg1];
assign read_data2 = reg_file[read_reg2];

//protection for reg0
assign reg_write_data = write_reg==0? 0: write_data;

endmodule
//! @title MIPS ID Stage
//! @author Maor.m
//! @version 0.1
//! @date 2022

`include "../mips_consts.sv"
`include "control.sv"
`include "hazzard_detect.sv"
`include "mips_registers.sv"
`include "../mips_structs.sv"
`timescale 1ns / 1ns

module id_stage_top (

    input [31:0] if_instruction,  //!input instruction
    input [31:0] if_next_pc,  //!next program counter
    // input [4:0] if_read_reg1,
    // input [4:0] if_read_reg2,

    output reg if_stall,

    output reg [31:0] ex_next_pc,  //!next program counter
    output reg [31:0] ex_read_data1,  //!read data 1
    output reg [31:0] ex_read_data2,  //!read data 2

    output reg [31:0] ex_imm,   //!immidate value extended to 32 bits
    output reg [ 4:0] ex_shamt, //shift amount for shift instructions

    output reg [4:0] ex_rs,  //!register source
    output reg [4:0] ex_rt,  //!register source
    output reg [4:0] ex_rd,  //!register destination

    input ex_memread,
    input [4:0] ex_rt_in,

    output reg [ 4:0] ex_control_to_m,   //!struct for passing signals to mem stage
    output reg [13:0] ex_control_to_ex,  //!struct for passing signals to ex stage
    output reg [ 1:0] ex_control_to_wb,  //!struct for passing signals to wb stage

    input mem_branch,  //!branch signal

    input wb_regwrite,  //!reg write signal
    input [4:0] wb_writereg,  //!reg write signal
    input [31:0] wb_writedata,  //!reg write signal

    input clk,  //!clock
    input rstn  //!reset

);

`ifdef COCOTB_SIM
  initial begin
    $dumpfile("id_stage_top.vcd");
    $dumpvars(0, id_stage_top);
    #1;
  end
  reg [31:0] test_number = 0;
`endif


  wire t_control_to_m controlm;
  wire t_control_to_ex controlex;
  wire t_control_to_wb controlwb;

  wire [31:0] imm_extended_unsigned;
  wire [31:0] imm_extended_signed;

  reg imm_unsigned;
  reg reg_imm;
  wire id_flush = mem_branch | if_stall;
  reg [31:0] read_data1;
  reg [31:0] read_data2;
  reg invalid_inst;
  reg [2:0] opcodetype;
  wire [31:0] jump_addr = {6'b0,if_instruction[25:0]} ;



  control control_dut (
      .op(if_instruction[31:26]),
      .funct(if_instruction[5:0]),
      .reg_imm_type(if_instruction[20:16]),
      .reg_dst(controlex.regdst),
      .reg_imm(reg_imm),
      .jump(controlex.jump),
      .link(controlex.link),
      .branch(controlm.branch),
      .branch_type(controlex.branchtype),
      .reg_write(controlwb.regwrite),
      .aluop(controlex.aluop),
      .alusrc(controlex.alusrc),
      .mem_write(controlm.memwrite),
      .mem_read(controlm.memread),
      .mem_toreg(controlwb.memtoreg),
      .mem_unsigned(controlm.memunsigned),
      .mem_op(controlex.memop),
      .imm_unsigned(imm_unsigned),
      .invalid_inst(invalid_inst),
      .cp0_reg_write(controlex.cp0_reg_write),
      .cp0_reg_read(controlm.cp0_reg_read),
      .cp0_subop(), //FIXME: ????
      .opcodetype(opcodetype),
      .rstn(rstn)
  );


  mips_registers mips_registers_dut (
      .read_reg1(if_instruction[25:21]),
      .read_reg2(if_instruction[20:16]),
      .write_reg(wb_writereg),
      .read_data1(read_data1),
      .read_data2(read_data2),
      .write_data(wb_writedata),
      .write_en(wb_regwrite),
      .clk(clk),
      .rstn(rstn)
  );

  hazzard_detect hazzard_detect_dut (
      .id_read_reg1(if_instruction[25:21]),
      .id_read_reg2(if_instruction[20:16]),
      .ex_memread(ex_memread),
      .ex_rt(ex_rt_in),
      .id_control_stall(if_stall)
  );

  // register pipeline
  always @(posedge clk) begin : id_stage_top_reg_pipeline
    if (!rstn) begin
      //clear all outputs
      ex_rd = 0;
      ex_rs = 0;
      ex_rt = 0;
      ex_read_data1 = 0;
      ex_read_data2 = 0;
      ex_control_to_ex = 0;
      ex_control_to_m = 0;
      ex_control_to_wb = 0;
      ex_imm = 0;
    end else begin
      // handle j instruction 
      if (controlex.jump) begin
        ex_imm = {6'b0, jump_addr};
      end else begin
        ex_imm = opcodetype == I_OPCODE ? (imm_unsigned? imm_extended_unsigned : imm_extended_signed):32'b0;
      end


      ex_control_to_m  = controlm;
      ex_control_to_ex = controlex;
      ex_control_to_wb = controlwb;
      ex_read_data2 <= reg_imm ? 32'b0 : read_data2;
      ex_rs = if_instruction[25:21];
      ex_rt = if_instruction[20:16];
      ex_rd = opcodetype == SPECIAL_OPCODE ? if_instruction[15:11] : 5'b0;
    end
  end

  assign imm_extended_signed   = {{16{if_instruction[15]}}, if_instruction[15:0]};
  assign imm_extended_unsigned = {16'b0, if_instruction[15:0]};
endmodule

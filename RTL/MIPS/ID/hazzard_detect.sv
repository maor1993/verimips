//! @title Hazzard Detect
//! @author Maor.m
//! @version 0.1
//! @date 2022



module hazzard_detect (
    input [4:0] id_read_reg1,
    input [4:0] id_read_reg2,

    input       ex_memread,
    input [4:0] ex_rt,

    output reg id_control_stall
);


always @(id_read_reg1,id_read_reg2,ex_rt,ex_memread) begin
    id_control_stall = 0;

    if (((id_read_reg1 == ex_rt)||(id_read_reg2 == ex_rt))&&ex_memread) begin
        id_control_stall = 1;
    end
end


endmodule

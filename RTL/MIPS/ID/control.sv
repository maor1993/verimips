//! @title Control
//! @author Maor.m
//! @version 0.1
//! @date 2022
// creates the many signals use to control the rest of pipeline, receives the current instruction from the IF Stage and outputs to EX and friends.

`include "../mips_consts.sv"
`include "../mips_structs.sv"
`timescale 1ns / 1ns


module control (
    input [5:0] op,  //! opcode used
    input [5:0] funct,  //! math function
    input [4:0] reg_imm_type,

    output reg reg_dst,
    output reg reg_imm,

    output reg jump,
    output reg link,
    output reg branch,
    output reg [2:0] branch_type,

    output reg reg_write,

    output reg [3:0] aluop,
    output reg alusrc,

    output reg mem_write,
    output reg mem_read,
    output reg mem_toreg,
    output reg mem_unsigned,
    output reg [1:0] mem_op,
    output reg imm_unsigned,
    output reg invalid_inst,

    output reg cp0_reg_write,
    output reg cp0_reg_read,
    output reg cp0_subop,

    output reg [2:0] opcodetype,


    input rstn

);

  // `ifdef COCOTB_SIM 
  //   initial begin
  //     $dumpfile("control.vcd");
  //     $dumpvars(0, control);
  //     #1;
  //   end
  //   reg [31:0] test_number = 0;
  // `endif

  import mips_consts::*;

  always @(rstn or op or funct or reg_imm_type) begin
    if (!rstn) begin
      reg_dst = 0;
      reg_imm = 0;
      jump = 0;
      link = 0;
      branch = 0;
      branch_type = c_btype_beq;
      reg_write = 0;
      aluop = c_aluop_add;
      alusrc = 0;
      mem_write = 0;
      mem_read = 0;
      mem_toreg = 0;
      mem_unsigned = 0;
      mem_op = c_memop_word;
      imm_unsigned = 0;
      invalid_inst = 0;
      cp0_reg_write = 0;
      cp0_reg_read = 0;
      cp0_subop = 0;
      opcodetype = SPECIAL_OPCODE;

    end else begin
      reg_dst = 0;
      reg_imm = 0;
      jump = 0;
      link = 0;
      branch = 0;
      branch_type = c_btype_beq;
      reg_write = 0;
      aluop = c_aluop_add;
      alusrc = 0;
      mem_write = 0;
      mem_read = 0;
      mem_toreg = 0;
      mem_unsigned = 0;
      mem_op = c_memop_word;
      imm_unsigned = 0;
      invalid_inst = 0;
      cp0_reg_write = 0;
      cp0_reg_read = 0;
      cp0_subop = 0;
      opcodetype = SPECIAL_OPCODE;
      if (op == c_op_special) begin
        case (funct)
          c_funct_jr: begin
            jump = 1;
            reg_dst = 1;
          end
          c_funct_jalr: begin
            jump = 1;
            reg_dst = 1;
            link = 1;
            reg_write = 1;
          end
          default: begin
            reg_dst = 1;
            reg_write = 1;
            aluop = c_aluop_special;
          end
        endcase
      end else begin
        opcodetype = I_OPCODE;
        case (op)
          c_op_addi: begin
            reg_write = 1;
            alusrc = 1;
            aluop = c_aluop_add;
          end
          c_op_addiu: begin
            reg_write = 1;
            alusrc = 1;
            imm_unsigned = 1;
            aluop = c_aluop_addu;
          end
          c_op_andi: begin
            reg_write = 1;
            alusrc = 1;
            imm_unsigned = 1;
            aluop = c_aluop_and;
          end
          c_op_ori: begin
            reg_write = 1;
            alusrc = 1;
            imm_unsigned = 1;
            aluop = c_aluop_or;
          end
          c_op_slti: begin
            reg_write = 1;
            alusrc = 1;
            aluop = c_aluop_slt;
          end
          c_op_sltiu: begin
            reg_write = 1;
            alusrc = 1;
            aluop = c_aluop_sltu;
          end
          c_op_lui: begin
            reg_write = 1;
            alusrc = 1;
            aluop = c_aluop_lui;
          end
          c_op_xori: begin
            reg_write = 1;
            alusrc = 1;
            imm_unsigned = 1;
            aluop = c_aluop_xor;
          end
          c_op_beq: begin
            branch = 1;
            branch_type = c_btype_beq;
            aluop = c_aluop_sub;
          end
          c_op_bne: begin
            branch = 1;
            branch_type = c_btype_bne;
            aluop = c_aluop_sub;
          end
          c_op_blez: begin
            branch = 1;
            branch_type = c_btype_blez;
            aluop = c_aluop_slte;
          end
          c_op_bgtz: begin
            branch = 1;
            branch_type = c_btype_bgtz;
            aluop = c_aluop_slte;
          end
          c_op_lb: begin
            mem_op = c_memop_byte;
            mem_read = 1;
            reg_write = 1;
            mem_toreg = 1;
            alusrc = 1;
            aluop = c_aluop_add;
          end
          c_op_lh: begin
            mem_read = 1;
            mem_op = c_memop_half;
            reg_write = 1;
            mem_toreg = 1;
            alusrc = 1;
            aluop = c_aluop_add;
          end
          c_op_lhu: begin
            mem_read = 1;
            mem_op = c_memop_half;
            reg_write = 1;
            mem_toreg = 1;
            mem_unsigned = 1;
            alusrc = 1;
            aluop = c_aluop_add;
          end
          c_op_lw: begin
            mem_read = 1;
            mem_op = c_memop_word;
            reg_write = 1;
            mem_toreg = 1;
            alusrc = 1;
            aluop = c_aluop_add;
          end
          c_op_lbu: begin
            mem_read = 1;
            mem_op = c_memop_byte;
            mem_unsigned = 1;
            reg_write = 1;
            mem_toreg = 1;
            alusrc = 1;
            aluop = c_aluop_add;
          end
          c_op_sb: begin
            alusrc = 1;
            mem_write = 1;
            aluop = c_aluop_add;
            mem_op = c_memop_byte;
          end
          c_op_sh: begin
            alusrc = 1;
            mem_write = 1;
            aluop = c_aluop_add;
            mem_op = c_memop_half;
          end
          c_op_sw: begin
            alusrc = 1;
            mem_write = 1;
            aluop = c_aluop_add;
            mem_op = c_memop_word;
          end
          c_op_cop0: begin
            if (cp0_subop) begin
              cp0_reg_write = 1;
            end else begin
              cp0_reg_read = 1;
              reg_write = 1;
            end
          end
          c_op_regimm: begin
            branch = 1;
            reg_imm = 1;
            aluop = c_aluop_slt;
            opcodetype = R_OPCODE;
            case (reg_imm_type)
              c_regimm_bgez: begin
                branch_type = c_btype_bgez;
              end
              c_regimm_bgezal: begin
                link = 1;
                reg_write = 1;
                branch_type = c_btype_bgez;
              end
              c_regimm_bltz: begin
                branch_type = c_btype_bltz;
              end
              c_regimm_bltzal: begin
                link = 1;
                reg_write = 1;
                branch_type = c_btype_bltz;
              end
              default: invalid_inst = 1;
            endcase
          end
          c_op_j: begin
            opcodetype = J_OPCODE;
            jump = 1;
          end
          c_op_jal: begin
            opcodetype = J_OPCODE;
            jump = 1;
            link = 1;
            reg_write = 1;
          end
          default: invalid_inst = 1;
        endcase
      end


    end

  end



endmodule

mips-mti-elf-objdump -h -t -d test.elf > dissasembly.txt
mips-img-elf-objcopy.exe -O ihex test.elf instmem.hex --only-section=.text --only-section=.except --pad-to=0x4000 
mips-img-elf-objcopy.exe -O ihex test.elf datamem.hex --only-section=.rodata --only-section=.data --pad-to=0x2000 --change-addresses=-0x10000

echo "done!"
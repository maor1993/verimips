/*
 * Utils.h
 *
 *  Created on: 21 ���� 2020
 *      Author: maor
 */
#ifndef UTILS_H_
#define UTILS_H_
#include <stdint.h>

#define CFG_BASE_ADDR 		(uint32_t*)0x3000

#define VERIFY_ADDR			CFG_BASE_ADDR + 0x4
#define TICK_ADDR			CFG_BASE_ADDR + 0x8
#define LEDS_ADDR			CFG_BASE_ADDR + 0x10
#define BUTTONS_ADDR		CFG_BASE_ADDR + 0x14

#define UART_TX_DATA_ADDR 	CFG_BASE_ADDR + 0x20
#define UART_TX_STATUS_ADDR CFG_BASE_ADDR + 0x24
#define UART_RX_DATA_ADDR	CFG_BASE_ADDR + 0x28
#define UART_RX_REQ_ADDR	CFG_BASE_ADDR + 0x2c
#define UART_RX_STATUS_ADDR CFG_BASE_ADDR + 0x30


#define IRQ_EN_ADDR			CFG_BASE_ADDR + 0x34
#define IRQ_MASK_ADDR		CFG_BASE_ADDR + 0x38
#define IRQ_STATUS_ADDR 	CFG_BASE_ADDR + 0x3c
#define IRQ_CLEAR_ADDR 		CFG_BASE_ADDR + 0x40


#define UART_STATUS_TX_DONE  0x1
#define UART_STATUS_RX_FULL  0x1
#define UART_STATUS_RX_EMPTY 0x2

#define TICKSTOMS(x) x*10






typedef struct div_result div_result;
struct div_result {
    int quotient;
    int remainder;
};


extern void out32(volatile uint32_t* nAddr,uint32_t i);
uint32_t in32 (volatile uint32_t* nAddr);
void print_to_console(const char* pMsg,int nSize);
void delay(uint32_t nTicks);
div_result divide(int dividend, int divisor);

void UartFlush();
int UartGetByte(char* pByte);


#endif

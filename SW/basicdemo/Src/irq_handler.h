/*
 * irq_handler.h
 *
 *  Created on: 2 ���� 2020
 *      Author: maor
 */

#ifndef IRQ_HANDLER_H_
#define IRQ_HANDLER_H_

#include <stdint.h>

typedef void (*irq_handler)();


typedef struct{
	irq_handler irq_handles[32];


}irq_handler_type;


/*
 * irq_enable
 * enables the cp0 irq trigger
 */
void irq_enable();


/*
 * set_irq_handler
 * connects the given irq index to a function pointer.
 */
void set_irq_handler(int nIrq,void* pFunc);




/*
 * handle_incoming_interrupt
 * this function is called when post irq store events, and will parse the current irq handle that needs to be called.
 */
void handle_incoming_interrupt();


#endif /* IRQ_HANDLER_H_ */

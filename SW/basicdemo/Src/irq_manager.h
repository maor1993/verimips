/*
 * irq_manager.h
 *
 *  Created on: 8 ���� 2020
 *      Author: maor
 */

#ifndef IRQ_MANAGER_H_
#define IRQ_MANAGER_H_

#include <stdint.h>
#include "Utils.h"

#define irq_manager_set_mask(mask) out32(IRQ_MASK_ADDR,mask)
#define irq_manager_enable() out32(IRQ_EN_ADDR,1)
#define irq_manager_disable() out32(IRQ_EN_ADDR,0)
#define irq_manager_clear_irq(clear) out32(IRQ_CLEAR_ADDR,clear)
#define irq_manager_get_status() in32(IRQ_STATUS_ADDR)


#endif /* IRQ_MANAGER_H_ */

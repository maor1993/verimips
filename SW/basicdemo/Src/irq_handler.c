/*
 * irq_handler.c
 *
 *  Created on: 2 ���� 2020
 *      Author: maor
 */

#include <stdint.h>
#include "irq_handler.h"
#include "irq_manager.h"
#include "Utils.h"


static volatile irq_handler_type sIrqs;



void set_irq_handler(int nIrq,void* pFunc)
{
	sIrqs.irq_handles[nIrq] = pFunc;
}

void irq_enable()
{
	__asm__(
		"li $k0,0x401\n\t"
		"mtc0 $k0,$12\n\t"
	);

}


void __attribute__((__section__(".except"))) handle_incoming_interrupt()
{
	//step1, read currently triggering irq.
	int nTrigIrq = irq_manager_get_status();
	int i;
	for(i=0;i<32;i++)
	{
		uint32_t idx = 1<<i;
		if((nTrigIrq&idx)==idx)
		{
			break;
		}
	}

	//step2, clear the interrupt we're about to handle.
	irq_manager_clear_irq(1<<i);

	//step3, if the irq was assigned a handler, call it.
	if(sIrqs.irq_handles[i])
	{
		sIrqs.irq_handles[i]();
	}


}

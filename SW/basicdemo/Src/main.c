/*
 * main.c
 *
 *  Created on: Jan 4, 2020
 *      Author: Maor
 */


#include <stdint.h>
#include "Utils.h"
#include "printf.h"
#include "irq_handler.h"
#include "irq_manager.h"

#define LISTSIZE 1000

extern int byte_array_dec[1000u];


void print_message()
{
	printf("irq happened!\n\r");
	delay(TICKSTOMS(1000));

}



void init_system()
{
	UartFlush();
}


static void setup_irqs()
{

	set_irq_handler(0,print_message);
	irq_manager_set_mask(0x1);


	irq_manager_enable();
	irq_enable();
}



static void print_menu()
{
	printf("What would you like to do?\n\r");
	printf("1.Write to leds\n\r");
	printf("2.Count to leds\n\r");
	printf("3.sort array\n\r");
	printf("4.enable Interrupts\n\r");
}


int main()
{
	char nSelection=0;
	char nData=0;

//	init_system();
	setup_irqs();


	printf("hello,world!\n\r");
	printf("MG MIPS V1\n\r");
	print_menu();
	while(1)
	{

		if(UartGetByte(&nSelection)==0)
		{
			switch(nSelection)
			{

			case '1':
			{
				printf("What to write?\n\r");
				while(UartGetByte(&nData));
				out32(LEDS_ADDR,nData);
				break;
			}
			case '2':
			{
				for(int i=0;i<10;i++)
				{
					out32((uint32_t*)LEDS_ADDR,i);
					printf("Writing to Leds %d\n\r",i);
					delay(TICKSTOMS(500));
				}
				break;
			}
			case '3':
			{
				uint32_t nStartTime=0;
				printf("Unsorted List %d Elements:\n\r",LISTSIZE);
				for(int i =0;i<LISTSIZE;i++)
				{
					printf("%d,",byte_array_dec[i]);
				}
				printf("\n\r");

				printf("Sorting...\n\r");
				nStartTime = in32(TICK_ADDR);
				quickSort(byte_array_dec,0,LISTSIZE-1);
				printf("Sort Took %d Ticks\n\r",in32(TICK_ADDR)-nStartTime);
				printf("Sorted List:\n\r");
				for(int i =0;i<LISTSIZE;i++)
				{
					printf("%d,",byte_array_dec[i]);
				}
				printf("\n\r");
				break;
			}
			case '4':
			{
				irq_enable();
				printf("Irq enabled!\n\r");
				break;
			}

			default:
			break;
			}
			print_menu();
		}
	}
	printf("All Done!, goodbye.\n\r");
	while(1);
}

/*
 * Utils.c
 *
 *  Created on: 21 ���� 2020
 *      Author: maor
 */

//#include <string.h>
#include "Utils.h"
#include <stdint.h>
#include <stddef.h>

void print_to_console(const char* pMsg,int nSize)
{
	//go through the string and wait for uart to complete
	for(int i=0;i<nSize;i++)
	{
		out32(UART_TX_DATA_ADDR,pMsg[i]);
		delay(1);
		while(in32(UART_TX_STATUS_ADDR)==0);
	}
}

uint32_t in32 (volatile uint32_t* nAddr)
{
	return *(nAddr);
}

inline void out32(volatile uint32_t* nAddr,uint32_t i)
{
	*(nAddr) = i;
}




void delay(uint32_t nTicks)
{
	uint32_t nStart = in32(TICK_ADDR);
	uint32_t nEnd = nStart + nTicks;
	uint32_t nCurr=0;

	do{
		nCurr = in32(TICK_ADDR);
	}while(nCurr<nEnd);
}

void _putchar(char character)
{
	out32(UART_TX_DATA_ADDR,character);
	delay(1);
	while(in32(UART_TX_STATUS_ADDR)==0);
}



#define INT_BITS (sizeof(int)*8)





div_result divide(int dividend, int divisor) {
    int negative = (dividend < 0) ^ (divisor < 0);
    div_result result;

    if (divisor == 0) {
        result.quotient = dividend < 0 ? INT32_MAX : INT32_MAX;
        result.remainder = 0;
        return result;
    }

    if ((dividend == INT32_MIN) && (divisor == -1)) {
        result.quotient = INT32_MAX;
        result.remainder = 0;
        return result;
    }

    if (dividend < 0) {
        dividend = -dividend;
    }
    if (divisor < 0) {
        divisor = -divisor;
    }

    int quotient = 0, remainder = 0;

    for (int i = 0; i < sizeof(int)*8; i++) {
        quotient <<= 1;

        remainder <<= 1;
        remainder += (dividend >> (INT_BITS - 1)) & 1;
        dividend <<= 1;

        if (remainder >= divisor) {
            remainder -= divisor;
            quotient++;
        }
    }


    if (negative) {
        result.quotient = -quotient;
        result.remainder = -remainder;
    } else {
        result.quotient = quotient;
        result.remainder = remainder;
    }
    return result;
}

void memcpy(void *dest, void *src,size_t n)
{
   // Typecast src and dest addresses to (char *)
   char *csrc = (char *)src;
   char *cdest = (char *)dest;

   // Copy contents of src[] to dest[]
   for (int i=0; i<n; i++)
       cdest[i] = csrc[i];
}


void UartFlush()
{
	out32(UART_RX_STATUS_ADDR,1);
}
int UartGetByte(char* pByte)
{
	out32(UART_RX_REQ_ADDR,1);
	uint32_t nData =in32(UART_RX_DATA_ADDR);

	if((nData&0x100)==0x100)
	{
		*pByte = nData&0xff;
		return 0;
	}
	else
	{
		return 1;
	}
}

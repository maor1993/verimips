
ADDR = 0x1000
IO = 0x3000

#stage1: basic checks
stage1:
li		$t1, 0x12345678		# $t1 = 0x12345678
li      $s1, ADDR
sw		$t1, 0($s1) 
sw      $t1, 4($s1)
lw      $t0, 0($s1) #expected t0 = 0x12345678
# lw      $t0, 1($s1) #should be error, not sure?
lw      $t2, 4($s1) #expected t2 = 0x12345678


#stage2: half words, unsigned half words
stage2: 
li      $t1, 0x8000 
li      $s1, ADDR
sw      $t1, 0($s1) 
lh      $t3, 0($s1) #expected t3 = 0xffff8000
lhu     $t4, 0($s1) #expected t4 = 0x00008000
sw      $t3, 0($s1)
lhu     $t7, 2($s1) #expted t4= 0x0000ffff
sh      $t7, 0($s1)
sh      $t7, 2($s1)
lw      $t5, 0($s1) #expected t5 = 0xffffffff

#stage3: bytes!
stage3:
li      $t1,0x12345678
li      $s1,ADDR
sw		$t1, 0($s1)		# 
lb      $t2, 3($s1)     #expected t2 = 0x12
lb      $t3, 2($s1)     #expected t3 = 0x34
lb      $t4, 1($s1)     #expected t4 = 0x56
lb      $t5, 0($s1)     #expected t5 = 0x78
sb      $t3, 3($s1)
sb      $t3, 2($s1)
sb      $t3, 1($s1)
sb      $t3, 0($s1)
lw      $t6, 0($s1)     #expected t6 = 0x34343434
li      $t1, 0xff
sb      $t1, 0($s1)
lb      $t3, 0($s1)     #expected t3 = 0xffffffff
lbu     $t4, 0($s1)     #expected t3 = 0x000000ff


#stage4: io
stage4:
li      $t1, 0x12345678
li      $s1, IO
sw      $t1, 0($s1) #expected intf_write =1 and intf_Data_out = 0x12345678
lw      $t1, 0($s1) #execpted intf_Read =1 and intf_Data_in = 0x00000000







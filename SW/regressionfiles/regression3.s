
#stage1 - j,jal,jr,jalr
main:li $s0,test1
nop
jalr $s0
li $s3,0x1234
nop
j end

test1: li $s2,0x4d
nop
jr $ra
end:nop

#stage2 - bgez 
li		$t1, 0x1234		
bgez   $t1,test1_bgez      
nop
nop
j nope1_bgez
test1_bgez: li $s1, 0x5678           #expected $s1=0x5678 
nope1_bgez: nop

li		$t1, 0		
bgez   $t1,test2_bgez
nop
nop
j nope2_bgez
test2_bgez: li $s1, 0x3345          #expected $s1=0x3345
nope2_bgez: nop

li		$t1, -2		
bgez   $t1,test3_bgez
nop
nop
j nope3_bgez
test3_bgez: li $s1, 0x7765          #expected $s1=0x3345
nope3_bgez: nop



#stage3 - beq
li	    $t1, 0x1234		
li	    $t2, 0x1234		
beq   $t1,$t2,test_beq
nop
nop
j nope1
test_beq: li $s1, 0x5678            #expected $s1=0x5678
nope1: nop


li	    $t1, 0x1234		
li	    $t2, 0x1233		
beq   $t1,$t2,test_beq_n
nop
nop
j nope2
test_beq_n: li $s1, 0x1234          #expected $s1=0x5678
nope2: nop


#stage4 - bne
li	    $t1, 0x1234		
li	    $t2, 0x5678		
bne   $t1,$t2,test_bne
nop
nop
j nope3
test_bne: li $s1, 0xaaaa             #expected $s1=0xaaaa
nope3: nop


li	    $t1, 0x5678		
li	    $t2, 0x5678		
bne   $t1,$t2,test_bne_n
nop
nop
j nope4
test_bne_n: li $s1, 0x5555           #expected $s1=0xaaaa
nope4: nop



#stage5 - bltz
li	    $t1, 0		
bltz   $t1,test_bltz1
nop
nop
j nope5
test_bltz1: li $s1, 0xa123             #expected $s1=0xaaaa
nope5: nop

li	    $t1, 0x15		
bltz   $t1,test_bltz2
nop
nop
j nope6
test_bltz2: li $s1, 0xa123             #expected $s1=0xaaaa
nope6: nop

li	    $t1, -3			
bltz   $t1,test_bltz3
nop
nop
j nope7
test_bltz3: li $s1, 0xa123           #expected $s1=0xa123
nope7: nop





#stage6 - bgtz
li		$t1, 0x1234		
bgtz   $t1,test1_bgtz      
nop
nop
j nope1_bgtz
test1_bgtz: li $s1, 0x5678           #expected $s1=0x5678 
nope1_bgtz: nop

li		$t1, 0		
bgtz   $t1,test2_bgtz
nop
nop
j nope2_bgtz
test2_bgtz: li $s1, 0x3345          #expected $s1=0x5678
nope2_bgtz: nop

li		$t1, -2		
bgtz   $t1,test3_bgtz
nop
nop
j nope3_bgtz
test3_bgtz: li $s1, 0x7765          #expected $s1=0x5678
nope3_bgtz: nop





#stage7 - blez
li		$t1, 0x1234		
blez   $t1,test1_blez      
nop
nop
j nope1_blez
test1_blez: li $s1, 0xaaaa           #expected $s1=0x5678 
nope1_blez: nop

li		$t1, 0		
blez   $t1,test2_blez
nop
nop
j nope2_blez
test2_blez: li $s1, 0x3345          #expected $s1=0x3345
nope2_blez: nop

li		$t1, -2		
blez   $t1,test3_blez
nop
nop
j nope3_blez
test3_blez: li $s1, 0x7765          #expected $s1=0x7765
nope3_blez: nop



#stage8 - bltzal
li		$t1, -2		
bltzal   $t1,test1_bltzal
nop
li      $s1,0x5a5a
nop
j end1
test1_bltzal: li $s1, 0xaaaa           #expected $s1=0xaaaa 
nop
jr $ra                                  #expected $s1=0x5a5a 
end1: nop


#stage9 - bgezal 
li		$t1, 0		
bgezal   $t1,test1_bgezal 
nop
li      $s1,0x2222
nop
j end2
test1_bgezal: li $s1, 0x1111           #expected $s1=0x1111
nop
jr $ra                                  #expected $s1=0x2222
end2: nop                              



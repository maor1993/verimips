stage1: addi $s1,-15 #(expected s1 = -15)
addi $s2,$zero,-1
addi $s2,-3 #(expected $s2 = -4)
andi $s2,0x12 #(expected s2= 0x10)
ori $s2,0x1 #(expected s2= 0x11)
xori $s2,0x11 #(expected s2= 0)

stage2:addi $s1,$zero,1
addi $s2,$zero,2
add $s1,$s1,$s2 #(expected s1 = 3)
addi $s1,$zero,-1
addu $s1,$s1,$s2 #(expected s1 = 1) (overflowed!)
sub	$s2, $s2, $s1	#(expected s2 = 1)
li $s1,0xfff89080
li $s2,0x12345678
subu $s1,$s1,$s2 #(expected s1 = 0xedc43a08)
and $s1,$s1,$s2 #(expected s1 = 0x41208)
or $s1,$s1,$s2 #(expected s1 = 0x12345678)
nor $s3,$s1,$s2 #(expected s3= 0xedcba987)
xor $s0,$s3,$s1 #(expected s0= 0xffffffff)

stage3: li $s1,0x1234
sll $s1,$s1,16 #(expected 0x12340000)
srl $s1,$s1,16 #(expected 0x1234)
li $s2,0x8
sllv $s1,$s1,$s2 #(expected 0x123400)
srlv $s1,$s1,$s2 #(expected 0x1234)
li $s3,-1234
sra $s3,$s3,0x4 #(expected 0xffffffb2)
li $s3,-1234
li $s0,0x4
srav $s3,$s3,$s0 #(expected 0xffffffb2)

stage4: slt $s0,$s3,$zero #(expected 1)
sltu $s0,$s3,$zero      #(expected 0)
slti $s0,$s3,-66 #(expected 1)
sltiu $s0,$s3,-80 #(expected 0)

stage5: li $s1,0x12341234
li $s2,0xA987A988
mult $s1,$s2
mfhi $s1 #(expected f9d9f353)
mflo $s2 #(expected f919ffa0)
